package com.devcamp.devcamp_car.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.devcamp_car.model.Car;



public interface ICarRespository extends JpaRepository<Car, Long>{

    // findByCarCode  đặt tên có quy tắc 
    Car findByCarCode(String car);   // muốn truy vấn phương thức này theo cách riêng
    //   findByCarCode tương đương getCarCode trong phần khai báo model
    
}
