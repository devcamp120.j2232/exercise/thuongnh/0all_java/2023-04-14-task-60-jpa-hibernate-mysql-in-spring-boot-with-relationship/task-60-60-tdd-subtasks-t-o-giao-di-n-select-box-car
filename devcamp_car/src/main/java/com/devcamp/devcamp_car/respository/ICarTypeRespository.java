package com.devcamp.devcamp_car.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.devcamp_car.model.CarType;

public interface ICarTypeRespository  extends JpaRepository<CarType,  Long> {
    
}
