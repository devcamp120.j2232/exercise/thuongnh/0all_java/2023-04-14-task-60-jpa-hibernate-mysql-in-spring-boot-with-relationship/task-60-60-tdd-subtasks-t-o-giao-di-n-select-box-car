package com.devcamp.devcamp_car;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevcampCarApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevcampCarApplication.class, args);
	}

}
