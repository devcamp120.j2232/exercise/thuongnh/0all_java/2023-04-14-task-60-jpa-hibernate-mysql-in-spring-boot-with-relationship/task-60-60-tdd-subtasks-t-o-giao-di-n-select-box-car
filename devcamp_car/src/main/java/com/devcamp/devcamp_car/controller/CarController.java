package com.devcamp.devcamp_car.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.devcamp_car.model.Car;
import com.devcamp.devcamp_car.model.CarType;
import com.devcamp.devcamp_car.respository.ICarRespository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CarController {

    // Viết API lấy danh sách car /devcamp-cars
    @Autowired
    ICarRespository pCarRespository;

    @GetMapping("/devcamp-cars")
    public ResponseEntity<List<Car>> getCarList(){
        try {
            List<Car> carList = new ArrayList<Car>();
            pCarRespository.findAll().forEach(carList::add);  //  lấy giá trị từ data base thêm vào carlist 
            return new ResponseEntity<>(carList, HttpStatus.OK);

        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    //Viết API lấy danh sách cartype truyền vào car code /devcamp-cartypes?carCode=

    

    @GetMapping("/devcamp-cartypes")
    public ResponseEntity<Set<CarType>> getCarTypeByCarCode(@RequestParam(value = "car_code") String carCode){
        try {
            Car vCar =  pCarRespository.findByCarCode(carCode);
            if(vCar != null){
                return new ResponseEntity<>(vCar.getTypes(), HttpStatus.OK); 
                 // vCar.getTypes()  trả về danh sách cartype tương ứng đã được gắn bên cơ sỏ dữ liệu 
            }
            else  {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND); //Nếu không tìm thấy thông tin về loại xe cho mã xe được truyền vào, phương thức sẽ trả về mã trạng thái HTTP 
                //là NOT_FOUND và giá trị của đối tượng ResponseEntity là null.
            }

        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            //để biểu thị rằng đã xảy ra lỗi phía máy chủ.
        }
    }
    
}
