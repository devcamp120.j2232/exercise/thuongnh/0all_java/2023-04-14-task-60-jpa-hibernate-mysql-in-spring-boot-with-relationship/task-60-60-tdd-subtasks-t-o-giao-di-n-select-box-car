package com.devcamp.devcamp_car.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

// định nghĩa entity trên model
@Entity
@Table(name = "car")
public class Car {
    // ánh xạ với bảng trên cơ sở dữ liệu 
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int  id;

    @Column(name = "car_code", unique = true)    //  unique = true có nghĩa car-code là duy nhất 
    private String carCode;

    @Column(name = "car_name")   // đây là ánh xạ 
    private String carName;

    
    @OneToMany(mappedBy = "car", cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<CarType> types;    // được truyền vào khi gán với cartype bên cơ sở dữ liệu 

	public Car() {
	}

	public Car(int id, String carCode, String carName, Set<CarType> types) {
		this.id = id;
		this.carCode = carCode;
		this.carName = carName;
		this.types = types;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCarCode() {
		return carCode;
	}

	public void setCarCode(String carCode) {
		this.carCode = carCode;
	}

	public String getCarName() {
		return carName;
	}

	public void setCarName(String carName) {
		this.carName = carName;
	}

	public Set<CarType> getTypes() {
		return types;
	}

	public void setTypes(Set<CarType> types) {
		this.types = types;
	}

    
    
    



}
