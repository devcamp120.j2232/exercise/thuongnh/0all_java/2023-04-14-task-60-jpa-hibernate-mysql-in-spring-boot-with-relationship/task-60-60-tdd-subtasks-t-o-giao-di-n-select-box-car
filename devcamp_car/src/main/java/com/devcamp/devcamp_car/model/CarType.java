package com.devcamp.devcamp_car.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity  // định nghĩa Entity 
@Table(name = "car_type")
public class CarType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int  id;

    @Column(name = "type_code", unique = true)
    private String typeCode;

    @Column(name = "type_name")
    private String typeName;

    @ManyToOne
	@JsonIgnore
    @JoinColumn(name="car_id")
    private Car car;

	public CarType() {
	}

	public CarType(int id, String typeCode, String typeName, Car car) {
		this.id = id;
		this.typeCode = typeCode;
		this.typeName = typeName;
		this.car = car;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}
    
    
    
}
